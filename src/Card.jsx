/* eslint-disable react/no-unescaped-entities */
import { useState, useEffect } from 'react';
import './Card.css';

const Card = ({projectDetail}) => {
    const [expanded, setExpanded] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setExpanded(true);
        }, 500);
    }, []);

    return (
        <div>
        {projectDetail ? 
        <div
            id='achievement-card'
            className={`card  ${expanded ? 'expanded' : ''}`}
        >
            <div className="content">
                <h2 className='card-title'>Achievement Card</h2>
                <p className='card-content'>
                    Yahooo! 🎊 You've cracked the code and made your mark on {projectDetail.projectName}! Your contribution is like a secret sauce in our coding recipe. 
                    Let's stir up more magic together!
                </p>
                <p className='card-awarded-to'>Awarded to: {projectDetail.author}</p>
            </div>
        </div>
        :
        <div className={`card`}>
            <div className="content">
                <p className='card-content'>Contribute to open source projects on gitlab to get your achievement certificate</p>
            </div>
        </div>
        }
        <div className='pattern-bg' >
            </div>
        </div>
    );
};

export default Card;
