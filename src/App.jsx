import './App.css'
import Card from './Card'
import confetti from 'canvas-confetti';
import { useEffect, useState } from 'react';
import downloadjs from 'downloadjs';
import html2canvas from 'html2canvas';

function App() {
  const [projectDetail, setProjectDetail] = useState(null);

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const dataStr = urlParams.get('kudos');

    if(dataStr){
        console.log(dataStr)
        const query = decodeURIComponent(window.atob(dataStr));
        const data = JSON.parse(query);
        setProjectDetail({projectName: data.pr, author: data.ah})
        console.log(data)
    }

    confetti({
      particleCount: 700,
      spread: 200,
      origin: {
        x: Math.random(),
        // since they fall down, start a bit higher than random
        y: Math.random() - 0.2,
      },
    });
  }, []);

  const handleCaptureClick = async () => {
    const downloadCerContent = document.querySelector('#achievement-card')
    const canvas = await html2canvas(downloadCerContent);
    const dataURL = canvas.toDataURL('image/png');
    downloadjs(dataURL, 'download.png', 'image/png');
  };

  return (
    <>
    <div>
    {projectDetail && <button className='download-button' onClick={handleCaptureClick}>Download</button>}
    </div>
    <Card projectDetail={projectDetail}/>
    </>
  )
}

export default App
